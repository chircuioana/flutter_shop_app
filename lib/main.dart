import 'package:flutter/material.dart';
import 'package:fluttershopapp/providers/auth.dart';
import 'package:fluttershopapp/providers/cart.dart';
import 'package:fluttershopapp/providers/orders.dart';
import 'package:fluttershopapp/providers/products.dart';
import 'package:fluttershopapp/screens/auth_screen.dart';
import 'package:fluttershopapp/screens/cart_screen.dart';
import 'package:fluttershopapp/screens/orders_screen.dart';
import 'package:fluttershopapp/screens/product_detail_screen.dart';
import 'package:fluttershopapp/screens/product_overview_screen.dart';
import 'package:fluttershopapp/screens/splash_sreen.dart';
import 'package:provider/provider.dart';

import 'helpers/CustomRoute.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => Auth()),
        ChangeNotifierProxyProvider<Auth, Products>(
          create: (context) => Products("", "", []),
          update: (context, authData, previous) => Products(
            authData.token,
            authData.userId,
            previous == null ? [] : previous.allProducts,
          ),
        ),
        ChangeNotifierProvider(create: (context) => Cart()),
        ChangeNotifierProvider(create: (context) => Orders()),
      ],
      child: Consumer<Auth>(
        builder: (context, authData, _) => MaterialApp(
          title: 'Flutter Shop App',
          theme: ThemeData(
            primarySwatch: Colors.pink,
            pageTransitionsTheme: PageTransitionsTheme(
              builders: {
                TargetPlatform.android: CustomPageTransitionBuilder(),
                TargetPlatform.iOS: CustomPageTransitionBuilder(),
              },
            ),
          ),
          home: authData.isAuth
              ? ProductOverviewScreen()
              : FutureBuilder(
                  future: authData.tryAutoLogin(),
                  builder: (ctx, authResultSnapshot) =>
                      authResultSnapshot.connectionState ==
                              ConnectionState.waiting
                          ? SplashScreen()
                          : AuthScreen(),
                ),
          routes: {
            ProductDetailScreen.routeName: (context) => ProductDetailScreen(),
            CartScreen.routeName: (context) => CartScreen(),
            OrdersScreen.routeName: (context) => OrdersScreen(),
//          AuthScreen.routeName: (context) => AuthScreen(),
          },
        ),
      ),
    );
  }
}
