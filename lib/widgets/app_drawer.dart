import 'package:flutter/material.dart';
import 'package:fluttershopapp/providers/auth.dart';
import 'package:fluttershopapp/screens/orders_screen.dart';
import 'package:fluttershopapp/screens/product_overview_screen.dart';
import 'package:provider/provider.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          AppBar(
            title: Text("Hello!"),
            automaticallyImplyLeading: false,
          ),
          InkWell(
            child: ListTile(
              leading: Icon(Icons.home),
              title: Text("Home"),
            ),
            onTap: () {
              Navigator.pushReplacementNamed(context, "/");
            },
          ),
          Divider(),
          InkWell(
            child: ListTile(
              leading: Icon(Icons.monetization_on),
              title: Text("Oders"),
            ),
            onTap: () {
              Navigator.pushReplacementNamed(context, OrdersScreen.routeName);
            },
          ),
          Divider(),
          InkWell(
            child: ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text("Logout"),
            ),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).pushReplacementNamed('/');
              Provider.of<Auth>(context, listen: false).logout();            },
          ),
        ],
      ),
    );
  }
}
