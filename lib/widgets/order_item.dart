import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttershopapp/providers/orders.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class OrderItem extends StatefulWidget {
  @override
  _OrderItemState createState() => _OrderItemState();
}

class _OrderItemState extends State<OrderItem> {
  var _expanded = false;

  @override
  Widget build(BuildContext context) {
    final orderData = Provider.of<OrderModel>(context);
    return Card(
      margin: EdgeInsets.all(10),
      child: Column(
        children: [
          ListTile(
            title: Text(
              "${orderData.total}\$",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
            subtitle: Text(
                DateFormat("dd/MM/yyyy hh:mm aa").format(orderData.orderedAt)),
            trailing: IconButton(
              icon: Icon(_expanded ? Icons.expand_less : Icons.expand_more),
              onPressed: () {
                setState(() {
                  _expanded = !_expanded;
                });
              },
            ),
          ),
          if (_expanded)
            Container(
              margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              height: orderData.cartItems.length * 20.0,
              child: ListView(
                children: orderData.cartItems
                    .map(
                      (cartItem) => Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            cartItem.product.title,
                            style: TextStyle(fontSize: 15),
                          ),
                          Text(
                            "${cartItem.quantity}x${cartItem.product.price}\$",
                            style: TextStyle(fontSize: 15, color: Colors.grey),
                          ),
                        ],
                      ),
                    )
                    .toList(),
              ),
            )
        ],
      ),
    );
  }
}
