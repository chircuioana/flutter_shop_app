import 'package:flutter/material.dart';
import 'package:fluttershopapp/providers/cart.dart';
import 'package:fluttershopapp/widgets/cart_item.dart';
import 'package:provider/provider.dart';

class CartList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cartData = Provider.of<Cart>(context);
    return ListView.separated(
      itemCount: cartData.cartSize,
      itemBuilder: (context, index) => ChangeNotifierProvider.value(
        value: cartData.itemsList[index],
        child: CartItem(),
      ),
      separatorBuilder: (context, index) {
        return Divider();
      },
    );
  }
}
