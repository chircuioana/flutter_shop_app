import 'package:flutter/material.dart';
import 'package:fluttershopapp/providers/product.dart';
import 'package:fluttershopapp/providers/products.dart';
import 'package:fluttershopapp/screens/product_overview_screen.dart';
import 'package:fluttershopapp/widgets/product_item.dart';
import 'package:provider/provider.dart';

class ProductsGrid extends StatelessWidget {
  final FilterOptions _filter;

  ProductsGrid(this._filter);

  @override
  Widget build(BuildContext context) {
//    final productsData = Provider.of<Products>(context);
//    final List<Product> products = _filter == FilterOptions.FAVORITES
//        ? productsData.favoriteProducts
//        : productsData.allProducts;
    return FutureBuilder(
      future:
          Provider.of<Products>(context, listen: false).fetchAndSetProducts(),
      builder: (ctx, dataSnapshot) {
        if (dataSnapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else {
          if (dataSnapshot.error != null) {
//            showDialog(
//              context: context,
//              builder: (context) => AlertDialog(
//                  title: Text("Error"),
//                  content: Text("${dataSnapshot.error.toString()}"),
//                  actions: [
////                    FlatButton(
////                      child: Text("Ok"),
////                      onPressed: () {
//////                        Navigator.of(context).pop(false);
////                      },
////                    )
//                  ]),
//            );
            return Center(
              child: Text('An error occurred!'),
            );
          } else {
            return Consumer<Products>(builder: (ctx, productsData, child) {
              final List<Product> products = _filter == FilterOptions.FAVORITES
                  ? productsData.favoriteProducts
                  : productsData.allProducts;
              return GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 3 / 4,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10),
                itemBuilder: (context, index) => ChangeNotifierProvider.value(
                  value: products[index],
                  child: ProductItem(),
                ),
                itemCount: products.length,
                padding: const EdgeInsets.all(10.0),
              );
            });
          }
        }
      },
    );
  }
}
