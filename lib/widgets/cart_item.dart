import 'package:flutter/material.dart';
import 'package:fluttershopapp/providers/cart.dart';
import 'package:provider/provider.dart';

class CartItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cartData = Provider.of<Cart>(context, listen: false);
    final cartElement = Provider.of<CartModel>(context);
    return Dismissible(
      key: ValueKey(cartElement.product.id),
      background: Container(
        padding: EdgeInsets.all(10),
        color: Colors.redAccent,
        child: Icon(
          Icons.delete,
          color: Colors.white,
        ),
        alignment: Alignment.centerRight,
      ),
      direction: DismissDirection.endToStart,
      onDismissed: (_) {
        cartData.removeItem(cartElement.product.id);
      },
      confirmDismiss: (_) {
        return showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  title: Text("Warning"),
                  content: Text("Are you sure you want to delete this item?"),
                  actions: [
                    FlatButton(
                      child: Text("No"),
                      onPressed: () {
                        Navigator.of(context).pop(false);
                      },
                    ),
                    FlatButton(
                      child: Text("Yes"),
                      onPressed: () {
                        Navigator.of(context).pop(true);
                      },
                    )
                  ],
                ));
      },
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        leading: Image.network(
          cartElement.product.imageUrl,
          fit: BoxFit.cover,
        ),
        title: Text(cartElement.product.title),
        subtitle: Text(cartElement.product.description),
        trailing: Text("${cartElement.quantity}x${cartElement.product.price}"),
      ),
    );
  }
}
