import 'package:flutter/material.dart';
import 'package:fluttershopapp/providers/auth.dart';
import 'package:fluttershopapp/providers/cart.dart';
import 'package:fluttershopapp/providers/product.dart';
import 'package:fluttershopapp/screens/product_detail_screen.dart';
import 'package:provider/provider.dart';
import 'package:transparent_image/transparent_image.dart';

class ProductItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final product = Provider.of<Product>(context);
    final cart = Provider.of<Cart>(context);
    final authData = Provider.of<Auth>(context, listen: false);
    return ClipRRect(
      borderRadius: BorderRadius.circular(8),
      child: GridTile(
        child: InkWell(
          onTap: () => Navigator.pushNamed(
              context, ProductDetailScreen.routeName,
              arguments: product.id),
          child: Hero(
            tag: product.id,
            child: FadeInImage(
              placeholder: MemoryImage(kTransparentImage),
              image: NetworkImage(product.imageUrl),
              fit: BoxFit.cover,
            ),
          ),
        ),
        footer: GridTileBar(
          backgroundColor: Colors.black38,
          title: Text(
            product.title,
            style: TextStyle(fontSize: 12),
          ),
          subtitle: Text(
            product.price.toString(),
            style: TextStyle(fontSize: 14),
          ),
          trailing: Row(
            children: <Widget>[
              Consumer<Product>(
                builder: (context, product, child) => InkWell(
                  child: Icon(
                    product.isFavorite ? Icons.favorite : Icons.favorite_border,
                    size: 15,
                  ),
                  onTap: () => {
                    product.toggleFavoriteStatus(
                      authData.token,
                      authData.userId,
                    )
                  },
                ),
              ),
              Container(
                width: 10,
              ),
              InkWell(
                child: Icon(
                  Icons.add_shopping_cart,
                  size: 15,
                ),
                onTap: () {
                  cart.addItem(product);
                  Scaffold.of(context).hideCurrentSnackBar();
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text("Added item to cart"),
                    duration: Duration(seconds: 3),
                    action: SnackBarAction(
                      label: "Undo",
                      onPressed: () {
                        cart.removeSingleItem(product.id);
                      },
                    ),
                  ));
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
