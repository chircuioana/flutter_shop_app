import 'package:flutter/foundation.dart';
import 'package:fluttershopapp/providers/product.dart';

class CartModel with ChangeNotifier {
  final Product product;
  int quantity = 1;

  CartModel({@required this.product});

  void incrementQuantity() {
    quantity += 1;
    notifyListeners();
  }
}

class Cart with ChangeNotifier {
  Map<String, CartModel> _items = {};

  Map<String, CartModel> get itemsMap {
    return {..._items};
  }

  List<CartModel> get itemsList {
    return [..._items.values];
  }

  int get cartSize {
    return _items.keys.length;
  }

  double get cartTotal {
    double total = 0.0;
    itemsList.forEach((element) {
      total += element.quantity * element.product.price;
    });
    return total;
  }

  void addItem(Product product) {
    if (_items.containsKey(product.id)) {
      final cartItem = _items[product.id];
      cartItem.incrementQuantity();
      _items.update(product.id, (value) => cartItem);
    } else {
      _items.putIfAbsent(product.id, () => CartModel(product: product));
    }
    notifyListeners();
  }

  void removeItem(String productId) {
    _items.remove(productId);
    notifyListeners();
  }

  void removeSingleItem(String productId) {
    if (!_items.containsKey(productId)) return;
    final cartItem = _items[productId];
    if (cartItem.quantity == 1) {
      _items.remove(productId);
    } else {
      cartItem.quantity = cartItem.quantity - 1;
      _items[productId] = cartItem;
    }
    notifyListeners();
  }

  void clearCart() {
    _items.clear();
    notifyListeners();
  }
}
