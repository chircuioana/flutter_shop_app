import 'package:flutter/material.dart';
import 'package:fluttershopapp/providers/product.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;

import '../models/http_exception.dart';

class Products with ChangeNotifier {
  Products(this._token, this._userId, this._products);

  final String _token;
  final String _userId;
  List<Product> _products = [
//    Product(
//        id: "1",
//        title: "Black Blouse",
//        description:
//            "Bonnie Blouse in Black by The Seamstress of Bloomsbury | Authentic Vintage 1940's Style",
//        price: 45.72,
//        imageUrl:
//            "https://i.pinimg.com/564x/28/63/db/2863db5afc241e6b20ea07eb60a65ab8.jpg"),
//    Product(
//        id: "2",
//        title: "Black Blouse Floral",
//        description: "Carolina Herrera | Moda Operandi",
//        price: 50.05,
//        imageUrl:
//            "https://i.pinimg.com/564x/3a/ad/82/3aad823185337794d260c3c706053b9d.jpg"),
//    Product(
//        id: "3",
//        title: "Black leather bolero",
//        description:
//            "BLACK MOON leather Bolero jacket with chains : vegan leather, genuine leather, cropped biker jacket, designer dark fashion, shrug jacket",
//        price: 99.99,
//        imageUrl:
//            "https://i.pinimg.com/564x/d8/c6/96/d8c6961041c48883a2a2144b97d4b0ab.jpg"),
//    Product(
//        id: "4",
//        title: "Floral dress",
//        description: "Sleeveless Mesh Top Flared Skater Dress AX41306ax",
//        price: 64.67,
//        imageUrl:
//            "https://i.pinimg.com/564x/56/79/72/567972ad4bcc1a41d45036e1864a7efe.jpg"),
//    Product(
//        id: "5",
//        title: "Pleated Shorts",
//        description: "Pleated Short by Miu Miu | Moda Operandi",
//        price: 108.99,
//        imageUrl:
//            "https://i.pinimg.com/564x/f9/f0/4c/f9f04cc26bb73f63c1019bcb0a43d1f4.jpg"),
//    Product(
//        id: "6",
//        title: "White Blouse",
//        description: "1940s Style Blouses, Tops, Shirts",
//        price: 78.45,
//        imageUrl:
//            "https://i.pinimg.com/564x/e0/e7/1a/e0e71af028917fe01db0fefccb22691b.jpg"),
//    Product(
//        id: "7",
//        title: "Ankle boots",
//        description:
//            "Living23 Embossed & Embroidery Block Heel Ankle Bootie w Faux Fur Lining",
//        price: 189.56,
//        imageUrl:
//            "https://i.pinimg.com/564x/8f/a4/9c/8fa49ce3ca4c79ab73f0bd501876a2dc.jpg"),
//    Product(
//        id: "8",
//        title: "Boots",
//        description: "Dapper Pointed Toe Lace-up Boots",
//        price: 108.99,
//        imageUrl:
//            "https://i.pinimg.com/564x/7f/ed/ea/7fedea463a49f68840f6f423d412a2a5.jpg")
  ];

  List<Product> get allProducts {
    return [..._products];
  }

  List<Product> get favoriteProducts {
    return [..._products.where((element) => element.isFavorite)];
  }

  Future<void> fetchAndSetProducts() async {
    final url =
        'https://shop-app-flutter-691f6.firebaseio.com/products.json?auth=$_token';
    try {
      final response = await http.get(url);
      final List<Product> loadedProducts = [];
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      if (extractedData == null) {
        return;
      }
      final favoritesUrl =
          'https://shop-app-flutter-691f6.firebaseio.com/userFavorites/$_userId.json?auth=$_token';
      final favoriteResponse = await http.get(favoritesUrl);
      final favoriteData = json.decode(favoriteResponse.body);
      extractedData.forEach((prodId, prodData) {
        loadedProducts.add(Product.fromNetwork(
          prodId,
          prodData,
          favoriteData == null ? false : favoriteData[prodId] ?? false,
        ));
      });
      _products = loadedProducts;
      notifyListeners();
    } catch (error) {
      throw (error);
    }
  }
}
