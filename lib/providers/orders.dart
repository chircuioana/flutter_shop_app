import 'package:flutter/foundation.dart';
import 'package:fluttershopapp/providers/cart.dart';

class OrderModel with ChangeNotifier {
  String id;
  List<CartModel> cartItems;
  DateTime orderedAt;

  double get total {
    double total = 0.0;
    cartItems.forEach((element) {
      total += element.quantity * element.product.price;
    });
    return total;
  }

  OrderModel({
    @required this.id,
    @required this.cartItems,
    @required this.orderedAt,
  });
}

class Orders extends ChangeNotifier {
  List<OrderModel> _orders = [];

  List<OrderModel> get orders {
    return [..._orders];
  }

  void addOrder(List<CartModel> cartItems) {
    _orders.insert(
        0,
        OrderModel(
          id: DateTime.now().toString(),
          cartItems: cartItems,
          orderedAt: DateTime.now(),
        ));
    notifyListeners();
  }
}
