import 'package:flutter/material.dart';
import 'package:fluttershopapp/providers/cart.dart';
import 'package:fluttershopapp/screens/cart_screen.dart';
import 'package:fluttershopapp/widgets/app_drawer.dart';
import 'package:fluttershopapp/widgets/badge.dart';
import 'package:fluttershopapp/widgets/products_grid.dart';
import 'package:provider/provider.dart';

enum FilterOptions {
  ALL,
  FAVORITES,
}

class ProductOverviewScreen extends StatefulWidget {
  @override
  _ProductOverviewScreenState createState() => _ProductOverviewScreenState();
}

class _ProductOverviewScreenState extends State<ProductOverviewScreen> {
  FilterOptions _filter = FilterOptions.ALL;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Shop"),
        actions: <Widget>[
          Consumer<Cart>(
            builder: (_, cart, child) {
              return Badge(
                child: child,
                value: "${cart.cartSize}",
              );
            },
            child: IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: () =>
                  {Navigator.pushNamed(context, CartScreen.routeName)},
            ),
          ),
          PopupMenuButton(
            icon: Icon(Icons.more_vert),
            itemBuilder: (_) => [
              PopupMenuItem(
                child: Text("Show All"),
                value: FilterOptions.ALL,
              ),
              PopupMenuItem(
                child: Text("Favorites"),
                value: FilterOptions.FAVORITES,
              ),
            ],
            onSelected: (FilterOptions selectedValue) {
              setState(() {
                _filter = selectedValue;
              });
            },
          ),
        ],
      ),
      body: ProductsGrid(_filter),
      drawer: AppDrawer(),
    );
  }
}
