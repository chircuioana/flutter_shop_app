import 'package:flutter/material.dart';
import 'package:fluttershopapp/providers/orders.dart';
import 'package:fluttershopapp/widgets/app_drawer.dart';
import 'package:fluttershopapp/widgets/order_item.dart';
import 'package:provider/provider.dart';

class OrdersScreen extends StatelessWidget {
  static const routeName = "/orders";

  @override
  Widget build(BuildContext context) {
    final ordersData = Provider.of<Orders>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Your orders"),
      ),
      body: ListView.builder(
        itemCount: ordersData.orders.length,
        itemBuilder: (context, index) => ChangeNotifierProvider.value(
          value: ordersData.orders[index],
          child: OrderItem(),
        ),
      ),
      drawer: AppDrawer(),
    );
  }
}
